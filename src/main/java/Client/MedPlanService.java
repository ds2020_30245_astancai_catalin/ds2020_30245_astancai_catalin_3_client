package Client;

import java.util.UUID;

public interface MedPlanService {
    String sendPlanToClient (UUID patientId);

}
