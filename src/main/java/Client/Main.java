package Client;

import View.UI;
import org.json.JSONObject;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.util.UUID;






@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class Main {


    public static void main(String[] args) {
        System.setProperty("java.awt.headless", "false");
        SpringApplicationBuilder springApplicationBuilder = new SpringApplicationBuilder(Main.class);
        MedPlanService medicationPlanService = springApplicationBuilder.run(args).getBean(MedPlanService.class);
        String object = medicationPlanService.sendPlanToClient(UUID.fromString("5a467bc7-c155-4226-8618-e76876cff5cf"));

        object = object.substring(1,object.length()-1);
        String[] msg = object.split(", ");


        System.out.println(msg[0]);
        System.out.println(msg[1]);



      /*  JSONObject g = new JSONObject();
         g.put("name",arrOfStr[1]);
         g.put("intake",arrOfStr[3]);

        */

        String title = new String("PillApp");
        UI userInterface = new UI(title,msg);
        userInterface.setVisible(true);
    }

}
