package View;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class UI extends JFrame{
    private JPanel mainPanel;
    private JTable table1;
    private JLabel clock;
    DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
    DateFormat currDate = new SimpleDateFormat("yyyy/MM/dd");
    public UI(String title, String[]msg){
        super(title);
        this.setContentPane(mainPanel);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        clock.setText(dateFormat.format(Calendar.getInstance().getTime()));



        ActionListener timerListener = new ActionListener()
        {
            public void actionPerformed(ActionEvent e) {
                String ora="17:46:00";
                clock.setText(dateFormat.format(Calendar.getInstance().getTime()));
                if(ora.equals(clock.getText())) {
                    String[] nameOfColumn = {"Medicament", "Intake"};
                    Object[][] data = new Object[msg.length][2];
                    for (int i = 0; i < msg.length; i++) {
                        String[] aux = msg[i].split(" ");
                        data[i][0] = aux[0];
                        data[i][1] = aux[1];

                    }
                    DefaultTableModel model = new DefaultTableModel(data, nameOfColumn);
                    table1.setModel(model);
                }
            }
        };

        Timer timer = new Timer(1000, timerListener);
        timer.setInitialDelay(0);
        timer.start();

    }
}
